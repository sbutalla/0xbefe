library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package project_config is
    
    constant PRJ_CFG_NUM_DMBS           : integer := 2;

end package project_config;

